# [2.0.0](https://bitbucket.org/dudufranz/teste-de-repositorio/compare/v1.2.0...v2.0.0) (2021-01-17)


### Features

* new feat test with a breaking change ([f9ac62f](https://bitbucket.org/dudufranz/teste-de-repositorio/commits/f9ac62f559af18d42a6b73585444ca1b51b3cb5e))


### BREAKING CHANGES

* new message provides a complete new User Experience

# [1.2.0](https://bitbucket.org/dudufranz/teste-de-repositorio/compare/v1.1.0...v1.2.0) (2021-01-17)


### Features

* add new feat teste ([80bed9b](https://bitbucket.org/dudufranz/teste-de-repositorio/commits/80bed9b634f97f7f573450243ba6b5773c04d82a))

# [1.1.0](https://bitbucket.org/dudufranz/teste-de-repositorio/compare/v1.0.0...v1.1.0) (2021-01-17)


### Features

* add new mensagem ([462cffe](https://bitbucket.org/dudufranz/teste-de-repositorio/commits/462cffe293b1a1d90584676970832c9dbc175aee))

# 1.0.0 (2021-01-17)


### Bug Fixes

* correcao erro gramatical ([1d2eae1](https://bitbucket.org/dudufranz/teste-de-repositorio/commits/1d2eae12d6f0b2d1e5972669907e0e0a07c0f4df))


### Features

* add mensagem inicial ([514b417](https://bitbucket.org/dudufranz/teste-de-repositorio/commits/514b41729967f0c6963164dcb7dfac193527b871))
* verifica se o numero e par ([4773212](https://bitbucket.org/dudufranz/teste-de-repositorio/commits/47732122e61cae09efbbc018de1155286b3a5fe9))


### Performance Improvements

* reducao de if desncessarios no metodo ([a80aef5](https://bitbucket.org/dudufranz/teste-de-repositorio/commits/a80aef5a0d921e2560464fb216e8519526dc82bd))


### Reverts

* teste revert commit ([16d8e02](https://bitbucket.org/dudufranz/teste-de-repositorio/commits/16d8e02ad9f108503695d15e414dfea7d8cbb1dd))
